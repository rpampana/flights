import React from 'react'
import { Form, message } from 'antd'
import{Link} from "react-router-dom"
import axios from 'axios'



function login() {
  const onFinish = async(values)=>{
    try {
      const response = await axios.post("api/users/login",values);
      if (response.data.sucess){
        message.sucess(response.data.message);

      }
      else{
        message.error(response.data.message)
      }
    } catch (error) {
      message.error(error.message)
    }
  }
  return (
    <div className='card'>
    <div className='table d-flex justify-content-center align-items-center' >
<div className='width card'>
  <h1>LOGIN</h1>
  <hr/>
<Form layout = 'verticlal' onFinish = {onFinish}>
        <Form.Item label = 'Name' name='name'>
     <input type = 'text'/>
        </Form.Item>

        <Form.Item label = 'Password' name='password'>
     <input type = 'Password'/>
        </Form.Item>
        <button className='login-btn' type = 'submit'>LOGIN</button>
     <Link to ="/register">register</Link>
     </Form>
     </div>
</div>
    </div>
  )
}

export default login