const mongoose = require('mongoose');

mongoose.connect(process.env.mongo_url);

const db = mongoose.connection;


db.on('connected', ()=>{
  console.log('mongo connection sucess');
})

db.on('error',() => {
  console.log('mongo connection failed');
});
